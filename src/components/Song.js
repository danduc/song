import React, { Component } from 'react';
import ReactDom from 'react-dom';
import SongList from './SongList';
import SongDetail from './SongDetail';

export default class Song extends Component{
    
    render()
    {
        return (
            <div>
               <h2>List of songs</h2>
                <div className='ui two column divided grid'>
                   <div className='ui row'>
                      <div className='column four wide'>
                         <SongList />
                      </div>
                     <div className='column two wide'>
                         <SongDetail />
                      </div>
                   </div>
                </div>
            </div>
         );
    }
}